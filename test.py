from checkmark import MarkdownForm

document = """
:[Username](username)
>[Username](username "Ex. @user:domain.tld")
:[Password](password)
>[Password](password "Choose something not too simple.")

@[Register](register)
"""

form = MarkdownForm(
    title='My Markdown Form',
    document=document
)

# Define a function to be called
# when you press the `Register` button.

@form.api_method
def register(data):
    username = data.get('username', "")
    password = data.get('password', "")

    form.update(['username', 'password'], data)

    print(f'Successfully registered as {username}')
    print(f'Your password is {"*" * len(password)}.')

@form.api_method
def update_email(data):
    layout = data.get('layout', 'modern')
    language = data.get('language', 'en')

    form.update(['email'], data)

    print(f"You've chosen the {layout} layout.")
    print(f'Language: {language}')


from rich import print
# print(form.elements)
print(form.html())
form.start()